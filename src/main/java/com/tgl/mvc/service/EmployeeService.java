package com.tgl.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgl.mvc.dao.EmployeeDao;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.util.DataUtil;

/**
 * 
 * @author kite.chen Designed for business logic
 *
 */
@Service
public class EmployeeService {

  @Autowired
  private EmployeeDao employeeDao;

  public long insert(Employee employee) {
    employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
    return employeeDao.insert(employee);
  }

  public boolean delete(long employeeId) {
    return employeeDao.delete(employeeId);
  }

  public Employee update(Employee employee) {
    employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
    return employeeDao.update(employee);
  }

  public Employee findById(long employeeId) {
    Employee result = employeeDao.findById(employeeId);
    if (result == null) {
      return null;
    }
    String chName = result.getChName();
    String maskedName = DataUtil.maskChName(chName);
    result.setChName(maskedName);
    return result;
  }
}
